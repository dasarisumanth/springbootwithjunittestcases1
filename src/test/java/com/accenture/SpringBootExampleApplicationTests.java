package com.accenture;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

//import java.util.List;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.accenture.model.Books;
import com.accenture.repository.BooksRepository;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
class SpringBootExampleApplicationTests {

	@Autowired
	BooksRepository bRepo;
	
	@Test
	@Order(1)
	public void testCreate () {
		Books b = new Books();
		b.setBookid(982);
		b.setBookname("Introduction to JavaSeries");
		b.setAuthor("Abraham ");
		b.setPrice(1700);
		bRepo.save(b);
		assertNotNull(bRepo.findById(982).get());
	}
	@Test
	@Order(2)
	public void testReadAll () {
		Iterable<Books> list = bRepo.findAll();
		assertThat(list).size().isGreaterThan(0);
	}
		
	@Test
	@Order(3)
	public void testRead () {
		Books books = bRepo.findById(982).get();
		assertEquals("Introduction to JavaSeries", books.getBookname());
	}
		
	@Test
	@Order(4)
	public void testUpdate () {
		Books b = bRepo.findById(982).get();
		b.setPrice(1700);
		bRepo.save(b);
		assertNotEquals(700, bRepo.findById(982).get().getPrice());
	}
		
	@Test
	@Order(5)
	public void testDelete () {
		bRepo.deleteById(982);
		assertThat(bRepo.existsById(982)).isFalse();
	}
}
